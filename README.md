First program for Course work of Programming for Parallel Computer Systems.

Purpose for the work: To create working example of program for Parallel Computer System with shared memory, that can calculate math function using threads and specific language with it's mechansim.
Programming language: Ada.
Used technologies: Ada's ISO standard stands for core mechanism - Protected modules. They can effectively store inside of them variables, and creates for them sequential control of the threads, so the work with them will be without mistakes and exceptions. Using of shared variables through Pragma Atomic also gives opportunity to stands with the sequential control of the threads, when all of them works with that variable.

Controling of the program:
Variable N stands for the size of Matrixes and Vectors respectively.
Variable P stands for the number of the threads, that program creates on the start, and works with them.
First thread will output the result.