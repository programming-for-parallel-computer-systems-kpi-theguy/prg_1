--------------------------PfPCS-------------------------
-----------------------Course Work----------------------
----PRG1. Ada. Protected modules and shared variables---
--------------------------------------------------------
--Task: e = max(t * X + B * (MA * MB))------------------
--------------------------------------------------------
--Author: Butskiy Yuriy, IO-52 group--------------------
--Date: 23.03.2018--------------------------------------
--------------------------------------------------------
generic
      N: in Positive;
      P: in Positive;
package data is
   
   type Vector is array (1..N) of Integer;
   type Vector_Atomic is array (1..N) of Integer;
   type Matrix is array (1..N) of Vector;
   type Matrix_Atomic is array (1..N, 1..N) of Integer;
   pragma Atomic_Components(Matrix_Atomic);
   
   H: Integer := N / P;
   
   --Input Integer, Vector, Matrix, atomic Vector, atomic Matrix
   procedure Input_Integer(a: out Integer);
   procedure Input_Vector(A: access Vector);
   procedure Input_Vector_Atomic(A: access Vector_Atomic);
   procedure Input_Matrix(MA: access Matrix);
   procedure Input_Matrix_Atomic(MA: access Matrix_Atomic);
   
   --Output Integer
   procedure Output_Integer(a: in Integer);
   
   --Multiply functions
   function Multiply_Matrixes(MA: access Matrix; MB: access Matrix_Atomic; k: in Natural) return access Matrix;
   function Multiply_Vector_Matrix(MA: access Matrix; A: access Vector_Atomic; k: in Natural) return access Vector;
   function Multiply_Vector_Integer(A: access Vector; b: in Integer; k: in Natural) return access Vector;
   
   --Sum procedure
   procedure Sum_Vectors(A: access Vector; B: access Vector; C: access Vector; k: in Natural);
   
   --Max function
   function Max_Vector(A: access Vector; k: in Natural) return Integer;
   
end data;
