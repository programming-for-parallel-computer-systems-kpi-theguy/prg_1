--------------------------PfPCS-------------------------
-----------------------Course Work----------------------
----PRG1. Ada. Protected modules and shared variables---
--------------------------------------------------------
--Task: e = max(t * X + B * (MA * MB))------------------
--------------------------------------------------------
--Author: Butskiy Yuriy, IO-52 group--------------------
--Date: 23.03.2018--------------------------------------
--------------------------------------------------------
with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar, Ada.Unchecked_Deallocation;
use Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar;
with data;

procedure Main is
   N: Positive := 4;
   P: Positive := 4;
   package using_data is new data(N, P);
   use using_data;

   --shared variables
   t: Integer;
   pragma Atomic(t);
   MA: access Matrix_Atomic := new Matrix_Atomic;
   B: access Vector_Atomic := new Vector_Atomic;

   --variables
   Z: access Vector := new Vector;
   X: access Vector := new Vector;
   MB: access Matrix := new Matrix;

   --time variable
   Time_Now: Time;
   Time_Duration: Duration;

   --protected module Module_Control
   protected Module_Control is
      entry Wait_Input;
      entry Wait_Calculation_max;

      procedure Signal_Input;
      procedure Signal_Calculation_max;

      procedure Max_E (a: in Integer);
      procedure Output_E;

   private
      e: Integer := Integer'First;
      F1: Natural := 0;
      F2: Natural := 0;
   end Module_Control;

   protected body Module_Control is
      entry Wait_Input when F1 = 2 is
      begin
         null;
      end Wait_Input;

      entry Wait_Calculation_max when F2 = P is
      begin
         null;
      end Wait_Calculation_max;

      procedure Signal_Input is
      begin
         F1 := F1 + 1;
      end Signal_Input;

      procedure Signal_Calculation_max is
      begin
         F2 := F2 + 1;
      end Signal_Calculation_max;

      procedure Max_E(a: in Integer) is
      begin
         e := Integer'Max(e, a);
      end Max_E;

      procedure Output_E is
  begin
         Output_Integer(e);
      end Output_E;
   end Module_Control;

   task type Tasks (Tid: Natural);

   task body Tasks is
      ei: Integer;
   begin
      Put_Line("T" & Integer'Image(Tid) & " started");

      --input X, MA, B, t, MB in T1 and TP
      if Tid = 1 then
         Input_Vector(X);
         Input_Matrix_Atomic(MA);

         --Signal about end of input in T1
         Module_Control.Signal_Input;
      end if;
      if Tid = P then
         Input_Vector_Atomic(B);
         Input_Integer(t);
         Input_Matrix(MB);

         --Signal about end of input in TP
         Module_Control.Signal_Input;
      end if;

      --wait for data input in T1 and TP
      Module_Control.Wait_Input;

      --Calculation of the algorithm
      Sum_Vectors(Multiply_Vector_Integer(X,t,Tid),Multiply_Vector_Matrix(Multiply_Matrixes(MB,MA,Tid),B,Tid),Z,Tid);

      --Calculation of local max
      ei := Max_Vector(Z, Tid);

      --Calculation of global max
      Module_Control.Max_E(ei);

      --Signal about end of calculation end
      Module_Control.Signal_Calculation_max;

      if Tid = 1 then
         --Wait for the end of calculation of max
         Module_Control.Wait_Calculation_max;

         --Output e in T1
         Module_Control.Output_E;
      end if;

      Put_Line("T" & Integer'Image(Tid) & " finished");
   end Tasks;

   type Task_Ptr is access Tasks;
   Tasks_arr: array (1..P) of Task_Ptr;

   --procedure to deallocate task after finishing
   procedure Free_Tasks is new Ada.Unchecked_Deallocation(Tasks, Task_Ptr);

   C: Character;
begin
   Get(C);
   Put_Line("PRG1 started");
   Time_Now := Clock;
   for i in 1..P loop
      Tasks_arr(i) := new Tasks(i);
   end loop;
   Module_Control.Wait_Calculation_max;
   Time_Duration := Clock - Time_Now;
   New_Line;
   delay 1.0;
   Put_Line("Time passed after start: " & Duration'Image(Time_Duration));
   for i in 1..P loop
      Free_Tasks(Tasks_arr(i));
   end loop;
   Put_Line("PRG1 finished");
end Main;
